---
# An instance of the Blank widget.
# Documentation: https://wowchemy.com/docs/getting-started/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 15

title: 
subtitle:

design:
  columns: "1"
  background:
    image: open-book.jpg
    image_darken: 1.0
    image_parallax: true
    image_position: center
    image_size: cover
    text_color_light: false
  spacing:
    padding: ["60px", "0", "40px", "0"]
---

How can R lovers like us get the hang of Python,
learn our way around, maybe… even love it?

We're writing a book to answer these questions.

Here we'll share progress updates,
and also leave a trail for others who hope
to write and publish technical books collaboratively in the open.

* [See the work in progress][wip]
* [Star the GitLab repo][gitlab-repo]
* [Join the conversation on Slack][slack]

[wip]: https://gitlab.com/python-from-r
[gitlab-repo]: https://gitlab.com/python-from-r/python-from-r.gitlab.io
[slack]: http://bit.ly/python-from-r-2021-feb-mar
