* Please use plant genomics datasets as examples too. _(6)_
* I learnt R base before learning tidyverse. I use both as R base could be easier sometimes _(8)_
* consider dvc for putting R and Python into a pipeline _(9)_
* Great idea for a book! Really excited to read it someday! _(10)_
* Besides completely ditching R for a set amount of time to completely immerse myself in python, I think this kind of crossover is the best method to learn. I learned SAS using a similar book. _(11)_
* Thank you for putting in the effort! It is very much appriciated! _(14)_
* No. _(16)_
* Thanks! _(18)_
* Just that I appreciate you guys doing this! _(27)_
* I’m excited about this book, but I don’t need it to be rushed. Take a healthy amount of time to get feedback. _(39)_
* I think this is an awesome initiative and I hope it will get the attention it deserves. It surely fills a gap that needs filling! Thank you! _(42)_
* I said I'd start reading the book today not because I want to put pressure on you but to highlight that it's an exciting project and I can't wait to see it being published. _(45)_
* Nothing else. _(50)_
* Thank  you so much for doing this! _(51)_
* Where do we follow your work and keep up to date with your efforts :) _(53)_
* my main hope is to be versatile, so that if I need to scale up python knowledge quickly, this book will let me use my R knowledge to help me do that _(58)_
* I'd like to know the authors of this book/project (no info on the survey!!). And please discuss about mansplaining practices from Python users (so different than R community), this perception is shared by me and my female peers. _(60)_
* You're super awesome for doing this! _(62)_
* Creating and using virtual environments. Demonstrate the code in an intuitive IDE (not PyCharm), possibly VS code or Sypder. _(64)_
* na _(68)_
* I answered the questionnaire but I'm already a Python user _(69)_
* This is something I’ve been looking for years for online. Thank you for starting this project! _(73)_
* This is something I’ve been looking for years for online. Thank you for starting this project! _(74)_
* Really looking forward to it!! _(79)_
* A lot of colleagues use Python & most useful would be to know how to translate many statistical models available in R into Python so that they can be more easily integrated into colleagues' workflow & codes. _(81)_
* I am very grateful for your initiative. Will you use bookdown.org format to publish online? _(82)_
* I am very grateful for your initiative. Will you use bookdown.org format to publish online? _(83)_
* Thank you and good luck! _(98)_
* The unusability of pythons path issues and difficulty of downloading packages really kill me. I end up just using Google Colab with jupyter notebooks but that's not good long term. Even anaconda only partly solved my issues. _(102)_
* Good luck! _(106)_
