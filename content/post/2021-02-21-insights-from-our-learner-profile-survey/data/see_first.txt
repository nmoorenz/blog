* environment management recommendations for someone who may have to use multiple versions _(1)_
* Data manipulation _(6)_
* Visualization _(7)_
* Python in rstudio _(8)_
* Creating, maintaining, and using virtual environments _(11)_
* Getting Python set up / IDE. I've heard installing from scratch is extremely painful compared to R. _(12)_
* pandas, scikit-learn, matplotlib/seaborn _(14)_
* I don't have a preference for a particular section. _(16)_
* Nop _(17)_
* Useful packages, Rmarkdown equivalent, using python in R Studio _(18)_
* Installing, getting started, and a very quick EDA. _(19)_
* IDE _(22)_
* Introduction to Python _(23)_
* Your view on when it makes most sense to use one language or another. _(25)_
* Just “getting started” Python is on my computer. I *think* my directories are set up right. Just need the next step. _(27)_
* Whatever seems intuitive! _(28)_
* None. Really appreciate ypu doing this. _(29)_
* Installing and configuring Python _(31)_
* Getting started, similar to R4DS :) _(32)_
* Preprocessing _(35)_
* Foundational Python, and tidyverse equivalents _(39)_
* n/a - not sure what sections would be available _(41)_
* Best practices in setting up a project (folder-structure, but also when to use classes vs. functions). _(42)_
* Data type _(43)_
* Language and data types - the basics! _(44)_
* object oriented programming would be great, as its not something usually done in R :) Thanks a lot, by the way its a great idea! _(46)_
* practical examples of data analysis _(48)_
* No preferences. _(50)_
* A combination of Python & R to create data pipelines. From extracting data via APIs to data transformation, data analysis & visualization, and finally a dynamic document like R Markdown + flexdashboard. Ideally, these are automated as far as possible. _(53)_
* Web development in Python, using frameworks like django and how they compare to shiny and R Markdown options _(56)_
* Getting started, installation, what's important to learn for data science _(57)_
* Python data types and introduction to oop _(59)_
* I won't ready it from Gitlab. But: Basic data management and plots. E.g. import a library, import a database, subset a database, boxplot, histogram, summary. _(60)_
* Incorporate Python into a R workflow _(61)_
* Oh please please the basics of how the syntax is different (e.g. periods have specific functions, what's a method, etc.). Help me get over the "how do I read this" hurdle, and I can branch into all the other excellent Python DS content out there. Thank you so much for this - as a working data scientist I don't have time for "Hello World" tutorials focused on web dev, but I do want to be able to review Pythonic colleagues' code! _(62)_
* Manipulating data frames in Python. _(63)_
* Machine Learning Model deployment in Python. _(64)_
* from start _(68)_
* A section on Python objects and on data manipulation. _(71)_
* Machine Learning _(72)_
* Initial Install + Environment setup + PIP - it is all so daunting and challenging to work through. _(73)_
* Initial Install + Environment setup + PIP - it is all so daunting and challenging to work through. _(74)_
* demystifying the diffs between R and Python; factors vs dummy variables; functions & loops _(79)_
* Linear models _(81)_
* As R user I would probably be tempted to try it out via reticulate in RStudio. I also cooperate with mostly excel users. For me it would be interesting to see first chapters on reading in and cleaning tabular data. Also it would be interesting to see Python structures being taught on tabular data from the start. _(82)_
* As R user I would probably be tempted to try it out via reticulate in RStudio. I also cooperate with mostly excel users. For me it would be interesting to see first chapters on reading in and cleaning tabular data. Also it would be interesting to see Python structures being taught on tabular data from the start. _(83)_
* no _(85)_
* Tyoes, code conventions and syntax _(86)_
* installing and managing a python environment _(87)_
* Integrating python code into R workflows _(89)_
* Integrating R with python in RStudio _(90)_
* virtual environment _(97)_
* Data manipulation _(104)_
* Python basics _(106)_
