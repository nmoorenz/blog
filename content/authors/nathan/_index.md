---
# Display name
title: Nathan Moore

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Data Engineer

# Organizations/Affiliations to show in About widget
organizations:
- name: Mercury NZ
  url: https://www.mercury.co.nz

# Short bio (displayed in user profile at end of posts)
bio: Nathan loves data visualisation, pub quiz, ultimate frisbee + disc golf, and exploring the neighbourhood. 

# Interests to show in About widget
interests:
- data visualisation
- sustainability
- chasing plastic

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/nmoorenz
  label: Follow me on Twitter
- icon: github
  icon_pack: fab
  link: https://github.com/nmoorenz
- icon: envelope
  icon_pack: fas
  link: 'mailto:nath@nmoore.nz'
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/nmoorenz/

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "a.message.to.nate@gmail.com"

# Highlight the author in author lists? (true/false)
highlight_name: true

user_groups:
- Meet the Team

authors:
- Nathan
---

Nathan is a data engineer at [Mercury][mercury] in Auckland, New Zealand. We're using Databricks on AWS to build a new company wide data platform. 

I'm a fan of #rstats, learning new things, and list comprehensions. I have organised [Taupo Hat][taupo] since 2009 and been involved in volunteering for [Auckland Ultimate][au] since 2007. 

I live in Panmure with my wife Dee, son Joe, and cat Panda. 

[mercury]: https://www.mercury.co.nz/
[au]: https://auckland.ultimatecentral.com/
[taupo]: https://www.taupohat.com
