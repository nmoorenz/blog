---
# Display name
title: Alberson Miranda

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Squad Coordinator

# Organizations/Affiliations to show in About widget
organizations:
- name: Banestes
  url: https://www.banestes.com.br

# Short bio (displayed in user profile at end of posts)
bio: Alberson is a data analytics chapter lead at the State Bank of ES-Brazil. He is an economist focused in econometrics and machine learning. He can be found at his home studio playing the guitar or surfing on the beach. Tweets <a href="https://twitter.com/AlbersonMiranda">@AlbersonMiranda</a>.

# Interests to show in About widget
interests:
- machine learning
- econometrics
- inferential statistics

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/AlbersonMiranda
  label: Follow me on Twitter
- icon: github
  icon_pack: fab
  link: https://github.com/albersonmiranda
- icon: envelope
  icon_pack: fas
  link: 'mailto:albersonmiranda@hotmail.com'
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/albersonmiranda/
- icon: cv
  icon_pack: ai
  link: https://datamares.netlify.app/files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "albersonmiranda@hotmail.com"

# Highlight the author in author lists? (true/false)
highlight_name: true

user_groups:
- Meet the Team

authors:
- alberson
---

Alberson is a data analytics chapter lead at the [State Bank of ES-Brazil](https://www.banestes.com.br). He is an economist focused in econometrics and machine learning.

Born in Rio de Janeiro, I live in Vitória, Brazil.
Whenever I can, I'll be at my home studio playing the guitar or surfing on the beach. I'm also a hoop head with questionable skills and a very slow go-karter.

I occasionally write (in pt-BR) on my [personal blog](https://datamares.netlify.app/).